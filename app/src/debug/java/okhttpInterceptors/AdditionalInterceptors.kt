package okhttpInterceptors

import android.content.Context
import com.facebook.stetho.Stetho
import com.facebook.stetho.okhttp3.StethoInterceptor
import np.pro.dipendra.trendingprojects.network.okhttp.ExtendedInterceptor
import okhttp3.Interceptor
import java.util.*


@Suppress("unused")
class AdditionalInterceptors : ExtendedInterceptor {

    override fun init(context: Context) {
        Stetho.initializeWithDefaults(context)
    }

    override fun getNetworkInterceptors(): List<Interceptor> {
        val interceptorList = ArrayList<Interceptor>()
        interceptorList.add(StethoInterceptor())
        return interceptorList
    }

}