package np.pro.dipendra.trendingprojects.dataprovider;

public interface DataProviderCallback<T> {
    void onRetrieved(T retrievedObject);

    void onFailed(DataProviderError dataProviderError);
}
