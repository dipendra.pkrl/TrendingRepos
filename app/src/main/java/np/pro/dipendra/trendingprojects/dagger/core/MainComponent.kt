package np.pro.dipendra.trendingprojects.dagger.core

import dagger.Component
import np.pro.dipendra.trendingprojects.dagger.modules.ContextModule
import np.pro.dipendra.trendingprojects.dagger.modules.DataProviderModule
import np.pro.dipendra.trendingprojects.dagger.modules.NetworkModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ContextModule::class, NetworkModule::class, DataProviderModule::class))
interface MainComponent : DaggerComponent