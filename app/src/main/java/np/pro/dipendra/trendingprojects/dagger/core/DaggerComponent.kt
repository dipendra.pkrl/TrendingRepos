package np.pro.dipendra.trendingprojects.dagger.core

import np.pro.dipendra.trendingprojects.activities.TrendingReposActivity
import np.pro.dipendra.trendingprojects.viewmodels.RepositoryListViewModel

interface DaggerComponent {
    fun inject(activity: TrendingReposActivity)
    fun inject(repositoryListViewModel: RepositoryListViewModel)
}