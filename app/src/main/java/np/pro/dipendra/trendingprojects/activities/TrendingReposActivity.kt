package np.pro.dipendra.trendingprojects.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ViewFlipper
import kotlinx.android.synthetic.main.activity_trending_repo.*
import np.pro.dipendra.trendingprojects.R
import np.pro.dipendra.trendingprojects.adapters.TrendingRepoAdapter
import np.pro.dipendra.trendingprojects.dataprovider.models.Repository
import np.pro.dipendra.trendingprojects.viewmodels.RepositoryListViewModel

class TrendingReposActivity : AppCompatActivity(), ListItemClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trending_repo)
        val listViewModel = setupViewModel()
        setupRetryFunctionality(listViewModel)
    }

    private fun setupRetryFunctionality(listViewModel: RepositoryListViewModel) {
        retryButton.setOnClickListener {
            viewFlipper.showView(progressBar)
            listViewModel.refreshRepos()
        }
    }

    private fun setupViewModel(): RepositoryListViewModel {
        viewFlipper.showView(progressBar)
        val listViewModel = ViewModelProviders.of(this).get(RepositoryListViewModel::class.java)
        listViewModel.getRepositories().observe(this, Observer<List<Repository>> {
            if (it == null) {
                viewFlipper.showView(errorView)
            } else {
                trendingRepoRecyclerView.adapter = TrendingRepoAdapter(it, this@TrendingReposActivity)
                viewFlipper.showView(trendingRepoRecyclerView)
            }
        })
        return listViewModel
    }


    private fun ViewFlipper.showView(view: View) {
        this.displayedChild = this.indexOfChild(view)

    }

    override fun onItemClicked(repository: Repository) {
        startActivity(RepoDetailActivity.newIntent(this, repository))
    }
}