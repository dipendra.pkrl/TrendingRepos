package np.pro.dipendra.trendingprojects.network.models.response

data class GithubRepository(val id: Int, val name: String, val full_name: String, val description: String, val html_url: String)
