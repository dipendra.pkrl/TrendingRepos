package np.pro.dipendra.trendingprojects.modules.interfaces.impl

import np.pro.dipendra.trendingprojects.dataprovider.DataProviderCallback
import np.pro.dipendra.trendingprojects.dataprovider.DataProviderError
import np.pro.dipendra.trendingprojects.dataprovider.models.Repository
import np.pro.dipendra.trendingprojects.modules.interfaces.TrendingRepoDataProvider
import np.pro.dipendra.trendingprojects.network.models.response.GithubRepository
import np.pro.dipendra.trendingprojects.network.models.response.GithubRepositoryResponse
import np.pro.dipendra.trendingprojects.network.retrofit.RetrofitCallback
import np.pro.dipendra.trendingprojects.network.retrofit.RetrofitError
import np.pro.dipendra.trendingprojects.network.retrofit.api.TrendingReposApi

class TrendingRepoDataProviderImpl(private val mTrendingReposApi: TrendingReposApi) : TrendingRepoDataProvider {

    override fun downloadRepositoriesInfo(dataProviderCallback: DataProviderCallback<List<Repository>>) {
        mTrendingReposApi.getRepositories().enqueue(object : RetrofitCallback<GithubRepositoryResponse>() {
            override fun onSuccess(responseGithub: GithubRepositoryResponse) {
                val githubRepos = responseGithub.items.map { it.toGithubRepository() }
                dataProviderCallback.onRetrieved(githubRepos)
            }

            override fun onFailure(retrofitError: RetrofitError) {
                dataProviderCallback.onFailed(DataProviderError(retrofitError.errorCode, retrofitError.errorMsg))
            }
        })
    }

    private fun GithubRepository.toGithubRepository() = Repository(id = id, name = name, full_name = full_name, description = description, html_url = html_url)
}