package np.pro.dipendra.trendingprojects.activities

import np.pro.dipendra.trendingprojects.dataprovider.models.Repository

interface ListItemClickListener {
    fun onItemClicked(repository: Repository)
}