package np.pro.dipendra.trendingprojects.dagger.core

import android.content.Context
import np.pro.dipendra.trendingprojects.dagger.modules.ContextModule
import np.pro.dipendra.trendingprojects.dagger.modules.NetworkModule

object DaggerWrapper {
    lateinit var component: DaggerComponent

    fun initComponent(context: Context) {
        component = DaggerMainComponent.builder()
                .contextModule(ContextModule(context))
                .networkModule(NetworkModule())
                .build()
    }
}