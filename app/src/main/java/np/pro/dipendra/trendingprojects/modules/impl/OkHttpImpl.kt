package np.pro.dipendra.trendingprojects.modules.impl

import android.content.Context
import np.pro.dipendra.trendingprojects.BuildConfig
import np.pro.dipendra.trendingprojects.modules.interfaces.Okhttp
import np.pro.dipendra.trendingprojects.network.okhttp.ExtendedInterceptor
import okhttp3.OkHttpClient

class OkHttpImpl(val context: Context) : Okhttp {
    override fun getOkHttpClient(): OkHttpClient {
        val httpBuilder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            val c = Class.forName("okhttpInterceptors.AdditionalInterceptors")
            val extendedInterceptor = c.newInstance() as ExtendedInterceptor
            extendedInterceptor.init(context)
            httpBuilder.networkInterceptors().addAll(extendedInterceptor.getNetworkInterceptors())
        }
        return httpBuilder.build()
    }
}