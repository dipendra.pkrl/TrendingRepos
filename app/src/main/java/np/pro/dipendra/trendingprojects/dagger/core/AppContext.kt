package np.pro.dipendra.trendingprojects.dagger.core

@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
annotation class AppContext
