package np.pro.dipendra.trendingprojects.dataprovider.models

import java.io.Serializable

data class Repository(val id: Int, val name: String?, val full_name: String?, val description: String?, val html_url: String?) : Serializable

