package np.pro.dipendra.trendingprojects.activities

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_repo_detail.*
import np.pro.dipendra.trendingprojects.R
import np.pro.dipendra.trendingprojects.dataprovider.models.Repository

class RepoDetailActivity : AppCompatActivity() {
    companion object {
        private var INTENT_ARG_REPO = "REPOSITORY"
        fun newIntent(context: Context, repository: Repository): Intent {
            return Intent(context, RepoDetailActivity::class.java)
                    .putExtra(INTENT_ARG_REPO, repository)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repo_detail)

        val repository = intent.getSerializableExtra(INTENT_ARG_REPO) as Repository
        repoTitle.text = repository.name
        repoDescription.text = repository.description
        btnSource.setOnClickListener { openLink(repository.html_url) }
    }

    private fun openLink(html_url: String?) {
        if (html_url != null) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(html_url)))
        } else {
            Toast.makeText(this, "Source  not found.", Toast.LENGTH_LONG).show()
        }
    }
}