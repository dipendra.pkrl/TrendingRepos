package np.pro.dipendra.trendingprojects

import android.app.Application
import np.pro.dipendra.trendingprojects.dagger.core.DaggerWrapper

class MainApp : Application() {
    override fun onCreate() {
        super.onCreate()
        DaggerWrapper.initComponent(this)
    }
}