package np.pro.dipendra.trendingprojects.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.trending_repo_item.view.*
import np.pro.dipendra.trendingprojects.activities.ListItemClickListener
import np.pro.dipendra.trendingprojects.R
import np.pro.dipendra.trendingprojects.dataprovider.models.Repository

class TrendingRepoAdapter(var mRepoList: List<Repository>, var mListItemClickListener: ListItemClickListener) : RecyclerView.Adapter<TrendingRepoAdapter.TrendingRepoViewHolder>() {
    override fun onBindViewHolder(holder: TrendingRepoViewHolder, position: Int) {
        holder.bind(mRepoList[position], mListItemClickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrendingRepoViewHolder {
        val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.trending_repo_item, parent, false)
        return TrendingRepoViewHolder(layoutView)
    }

    override fun getItemCount(): Int {
        return mRepoList.size
    }

    class TrendingRepoViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(repository: Repository, listItemClickListener: ListItemClickListener) {
            view.repoTitle.text = repository.name
            view.repoDescription.text = repository.description
            view.setOnClickListener { listItemClickListener.onItemClicked(repository) }
        }
    }
}