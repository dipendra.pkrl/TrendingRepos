package np.pro.dipendra.trendingprojects.network.okhttp

import android.content.Context
import okhttp3.Interceptor

interface ExtendedInterceptor {
    fun init(context: Context)
    fun getNetworkInterceptors(): List<Interceptor>
}