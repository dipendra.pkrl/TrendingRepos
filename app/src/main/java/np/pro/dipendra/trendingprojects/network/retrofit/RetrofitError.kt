package np.pro.dipendra.trendingprojects.network.retrofit

class RetrofitError(val errorCode: Int, val errorMsg: String? = null) {

    companion object {
        val ERROR_NETWORK_FAILURE = 1001
    }


}