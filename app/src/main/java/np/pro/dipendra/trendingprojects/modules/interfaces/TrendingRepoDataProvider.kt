package np.pro.dipendra.trendingprojects.modules.interfaces

import np.pro.dipendra.trendingprojects.dataprovider.DataProviderCallback
import np.pro.dipendra.trendingprojects.dataprovider.models.Repository

interface TrendingRepoDataProvider {
    fun downloadRepositoriesInfo(dataProviderCallback: DataProviderCallback<List<Repository>>)
}