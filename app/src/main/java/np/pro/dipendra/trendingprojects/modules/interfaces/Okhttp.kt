package np.pro.dipendra.trendingprojects.modules.interfaces

import okhttp3.OkHttpClient

interface Okhttp {
    fun getOkHttpClient(): OkHttpClient
}