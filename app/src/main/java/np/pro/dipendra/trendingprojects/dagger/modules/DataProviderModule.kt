package np.pro.dipendra.trendingprojects.dagger.modules

import dagger.Module
import dagger.Provides
import np.pro.dipendra.trendingprojects.modules.interfaces.TrendingRepoDataProvider
import np.pro.dipendra.trendingprojects.modules.interfaces.impl.TrendingRepoDataProviderImpl
import np.pro.dipendra.trendingprojects.network.retrofit.api.TrendingReposApi
import javax.inject.Singleton

@Module
class DataProviderModule {
    @Singleton
    @Provides
    fun provideTrendingRepoDataProvider(trendingReposApi: TrendingReposApi): TrendingRepoDataProvider {
        return TrendingRepoDataProviderImpl(trendingReposApi)
    }
}