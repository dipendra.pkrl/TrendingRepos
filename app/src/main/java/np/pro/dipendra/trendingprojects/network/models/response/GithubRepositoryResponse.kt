package np.pro.dipendra.trendingprojects.network.models.response

class GithubRepositoryResponse(val items: List<GithubRepository>)