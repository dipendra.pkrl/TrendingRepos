package np.pro.dipendra.trendingprojects.dagger.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import np.pro.dipendra.trendingprojects.dagger.core.AppContext

@Module
class ContextModule(@AppContext context: Context) {
    private val mContext: Context = context.applicationContext

    @Provides
    @AppContext
    internal fun provideContext(): Context {
        return mContext
    }
}