package np.pro.dipendra.trendingprojects.dataprovider

class DataProviderError(val errorCode: Int, val errorMsg: String? = null)