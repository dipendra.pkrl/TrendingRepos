package np.pro.dipendra.trendingprojects.network.retrofit.api

import np.pro.dipendra.trendingprojects.network.models.response.GithubRepositoryResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface TrendingReposApi {

    @GET("search/repositories?q=topic:android+language:java+language:kotlin&sort=stars&order=desc")
    fun getRepositories(@Query("page") page: Int = 0): Call<GithubRepositoryResponse>
}