package np.pro.dipendra.trendingprojects.dagger.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import np.pro.dipendra.trendingprojects.dagger.core.AppContext
import np.pro.dipendra.trendingprojects.modules.impl.OkHttpImpl
import np.pro.dipendra.trendingprojects.modules.interfaces.Okhttp
import np.pro.dipendra.trendingprojects.network.retrofit.api.TrendingReposApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    internal fun provideOkHttp(@AppContext context: Context): Okhttp {
        return OkHttpImpl(context)
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(okhttp: Okhttp): Retrofit {
        return Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okhttp.getOkHttpClient())
                .build()
    }

    @Provides
    @Singleton
    internal fun provideTrendingRepoApi(retrofit: Retrofit): TrendingReposApi {
        return retrofit.create(TrendingReposApi::class.java)
    }

}