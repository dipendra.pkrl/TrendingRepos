package np.pro.dipendra.trendingprojects.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import np.pro.dipendra.trendingprojects.dagger.core.DaggerWrapper
import np.pro.dipendra.trendingprojects.dataprovider.DataProviderCallback
import np.pro.dipendra.trendingprojects.dataprovider.DataProviderError
import np.pro.dipendra.trendingprojects.dataprovider.models.Repository
import np.pro.dipendra.trendingprojects.modules.interfaces.TrendingRepoDataProvider
import javax.inject.Inject


class RepositoryListViewModel : ViewModel() {
    @Inject protected lateinit var mTrendingRepoDataProvider: TrendingRepoDataProvider

    private var repositories: MutableLiveData<List<Repository>>? = null

    init {
        DaggerWrapper.component.inject(this)
    }

    fun getRepositories(): LiveData<List<Repository>> {
        if (repositories == null) {
            repositories = MutableLiveData()
            loadRepos()
        }
        return repositories!!
    }

    fun refreshRepos() {
        loadRepos()
    }

    private fun loadRepos() {
        mTrendingRepoDataProvider.downloadRepositoriesInfo(object : DataProviderCallback<List<Repository>> {
            override fun onFailed(dataProviderError: DataProviderError?) {
                repositories!!.value = null
            }

            override fun onRetrieved(retrievedObject: List<Repository>) {
                repositories!!.value = retrievedObject
            }
        })

    }


}