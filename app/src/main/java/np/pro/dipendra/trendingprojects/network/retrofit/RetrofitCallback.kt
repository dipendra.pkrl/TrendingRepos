package np.pro.dipendra.trendingprojects.network.retrofit

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

abstract class RetrofitCallback<T> : Callback<T> {
    abstract fun onSuccess(response: T)
    abstract fun onFailure(retrofitError: RetrofitError)

    final override fun onFailure(call: Call<T>?, t: Throwable?) {
        onFailure(RetrofitError(RetrofitError.ERROR_NETWORK_FAILURE, t?.localizedMessage))
    }

    final override fun onResponse(call: Call<T>?, response: Response<T>?) {
        if (response == null || !response.isSuccessful || response.body() != null) {
            onSuccess(response!!.body()!!)
        } else {
            onFailure(call, Throwable("Response body not found."))
        }
    }
}